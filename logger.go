package main

type Logger interface {
	Printf(format string, v ...interface{})
}

package main

import (
	"flag"
	"fmt"

	col "github.com/hamza02x/go-color"
)

var (
	dbPath            string // flag
	proxyPort         int    // flag
	communicationPort int    // flag
)

func flags() {
	flag.IntVar(&proxyPort, "proxy-port", 8080, "proxy port")
	flag.IntVar(&communicationPort, "communication-port", 3001, "communication port")
	flag.StringVar(&dbPath, "db-path", "/Users/nix/code/qt/bilai-suite/test.db", "database path")

	flag.Parse()

	fmt.Println(col.Red("[flags parsed]"))

	flag.VisitAll(func(f *flag.Flag) {
		fmt.Printf("-%s: %s\n", f.Name, col.Info(f.Value))
	})

	fmt.Println(col.Red("[end flags parsed]"))
}

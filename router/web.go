package router

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/hamza02x/bilai-proxy/cfg"
	"gitlab.com/hamza02x/bilai-proxy/service"
)

func GetWebRouter() *gin.Engine {

	r := gin.New()

	if !cfg.IsLocal {
		gin.SetMode(gin.ReleaseMode)
	}

	r.Use(gin.Recovery())

	r.GET("/ws", ws)

	r.GET("/intercept", intercept)
	r.GET("/", home)
	r.GET("/cert", cert)

	// ex: format_by_id?request_id=23&part=request
	// ex: format_by_id?request_id=23&part=response
	r.GET("/format_by_id", formatById)

	r.POST("/graphql", service.DoGraphQlQuery)

	return r

}

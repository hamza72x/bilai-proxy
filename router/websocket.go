package router

import (
	"encoding/json"

	hel "github.com/hamza02x/go-helper"
	"golang.org/x/net/websocket"
)

type SocketMessage struct {
	Data    string `json:"data"`
	Command string `json:"command"`
}

func waitForSocketMessage(c *websocket.Conn) interface{} {

	var message string
	websocket.Message.Receive(c, &message)

	hel.Pl("Received", message)

	sm := parseSocketMessage(message)

	hel.Pl("sm", sm)

	return nil
}

func parseSocketMessage(str string) SocketMessage {
	var sm SocketMessage
	json.Unmarshal([]byte(str), &sm)
	return sm
}

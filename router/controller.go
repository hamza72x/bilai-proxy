package router

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	hel "github.com/hamza02x/go-helper"
	"gitlab.com/hamza02x/bilai-proxy/consts"
	"gitlab.com/hamza02x/bilai-proxy/db"
	"gitlab.com/hamza02x/bilai-proxy/formatter"
	"gitlab.com/hamza02x/bilai-proxy/model"
	"golang.org/x/net/websocket"
)

func formatById(c *gin.Context) {

	var err error
	var id = c.Query("request_id")

	var rx model.RequestX

	db.GetConn().Where("id = ?", id).First(&rx)

	if c.Query("part") == "response" {

		var respx model.ResponseX

		db.GetConn().Where("request_id = ?", rx.ID).First(&respx)

		respx.Beautify(rx.Type)

		err = formatter.FormatHTTP(c.Writer, string(respx.Header+"\r\n\r\n"+respx.Body))

	} else {

		err = formatter.FormatHTTP(c.Writer, rx.GetRequestString())

	}

	if err != nil {
		fmt.Println("Error in format", err)
	}

}

func intercept(c *gin.Context) {

	if hel.FileExists(consts.FILE_INTERCEPT) {
		hel.FileRemoveIfExists(consts.FILE_INTERCEPT)
	} else {
		hel.StrToFile(consts.FILE_INTERCEPT, "")
	}

	if hel.FileExists(consts.FILE_INTERCEPT) {
		c.String(http.StatusOK, "on")
	} else {
		c.String(http.StatusOK, "off")
	}

}

func home(c *gin.Context) {
	c.String(http.StatusOK, "TODO")
}

func cert(c *gin.Context) {
	c.String(http.StatusOK, "TODO: EMBED file in binary?")

	// c.Header("Content-Type", "application/octet-stream")
	// c.Header("Content-Disposition", "attachment; filename=bilaiproxy.pem")

	// c.File("certs/bilaiproxy.pem")
	// c.Data(http.StatusOK, "application/octet-stream", certs.CA_CERT)
}

func ws(c *gin.Context) {

	handler := websocket.Handler(func(c *websocket.Conn) {
		for {
			websocket.Message.Send(c, waitForSocketMessage(c))
		}
	})

	handler.ServeHTTP(c.Writer, c.Request)
}

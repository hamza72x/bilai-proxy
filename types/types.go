package types

import (
	"github.com/graphql-go/graphql"
	"gitlab.com/hamza02x/bilai-proxy/resolvers"
)

var RequestXType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Request",
	Fields: graphql.Fields{
		"id":     &graphql.Field{Type: graphql.Int},
		"host":   &graphql.Field{Type: graphql.String},
		"method": &graphql.Field{Type: graphql.String},
		"path":   &graphql.Field{Type: graphql.String},
		"name":   &graphql.Field{Type: graphql.String},
		"length": &graphql.Field{Type: graphql.Int},
		"type":   &graphql.Field{Type: graphql.String},
		"header": &graphql.Field{Type: graphql.String},
		"body":   &graphql.Field{Type: graphql.String},
		"response": &graphql.Field{
			Type:    ResponseXType,
			Resolve: resolvers.ResolverResponseXFromRequestX,
		},
	},
})

var ResponseXType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Response",
	Fields: graphql.Fields{
		"header": &graphql.Field{Type: graphql.String},
		"body":   &graphql.Field{Type: graphql.String},
	},
})

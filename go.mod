module gitlab.com/hamza02x/bilai-proxy

go 1.16

require (
	github.com/alecthomas/chroma v0.9.1
	github.com/dgraph-io/ristretto v0.0.3
	github.com/gin-gonic/gin v1.7.1
	github.com/graphql-go/graphql v0.7.9 // indirect
	github.com/hamza02x/go-color v1.0.0
	github.com/hamza02x/go-helper v1.2.3
	github.com/itchyny/gojq v0.12.3
	github.com/itchyny/timefmt-go v0.1.3 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/yosssi/gohtml v0.0.0-20201013000340-ee4748c638f4 // indirect
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gorm.io/driver/sqlite v1.1.4
	gorm.io/gorm v1.21.9
)

package methods

import (
	"net/url"
	"os"
	"path/filepath"
	"strings"
)

// RemoveAllContents removes all contents recursively
// beware!!!!!!!!!!!!!
func RemoveAllContents(dir string) error {
	d, err := os.Open(dir)
	if err != nil {
		return err
	}
	defer d.Close()
	names, err := d.Readdirnames(-1)
	if err != nil {
		return err
	}
	for _, name := range names {
		err = os.RemoveAll(filepath.Join(dir, name))
		if err != nil {
			return err
		}
	}
	return nil
}

// returns exmaple: /v1/api?id=2#buddy
func GetPathV1OfUrl(u *url.URL) string {

	var buf strings.Builder

	if u.Opaque != "" {

		buf.WriteString(u.Opaque)

	} else {

		path := u.EscapedPath()
		if path != "" && path[0] != '/' && u.Host != "" {
			buf.WriteByte('/')
		}
		if buf.Len() == 0 {
			if i := strings.IndexByte(path, ':'); i > -1 && strings.IndexByte(path[:i], '/') == -1 {
				buf.WriteString("./")
			}
		}
		buf.WriteString(path)
	}

	if u.ForceQuery || u.RawQuery != "" {
		buf.WriteByte('?')
		buf.WriteString(u.RawQuery)
	}

	if u.Fragment != "" {
		buf.WriteByte('#')
		buf.WriteString(u.EscapedFragment())
	}

	return buf.String()
}

/*
import (
	"bytes"
	"io/ioutil"
	"net/http"
)

var (
	// CRLF = `\r\n` // for debugging
)

type HttpRequestX struct {
	*http.Request
}

type HttpResponseX struct {
	*http.Response
}
*/
/*
GET /get HTTP/1.1
Host: httpbin.org
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*\/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Accept-Encoding: gzip, deflate
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
Connection: close
*/
/*
func (r *HttpRequestX) getRawHttp() string {

	var str = r.Method + " " + r.URL.Path + " " + r.Proto + CRLF

	str += "Host: " + r.Host + CRLF

	for name, values := range r.Header {
		count := len(values)
		for i, value := range values {
			str += name + ": " + value
			if i+1 != count {
				str += "; "
			}
		}
		str += CRLF
	}

	str += CRLF

	bodyBytes, err := ioutil.ReadAll(r.Body)

	if err == nil {
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
		str += string(bodyBytes)
	}

	return str
}

/*
HTTP/1.1 200 OK
Date: Thu, 01 Apr 2021 03:47:23 GMT
Content-Type: application/json
Content-Length: 622
Connection: close
Server: gunicorn/19.9.0
Access-Control-Allow-Origin: *
Access-Control-Allow-Credentials: true

{
  "args": {},
  "headers": {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*\/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
    "Accept-Encoding": "gzip, deflate",
    "Accept-Language": "en-GB,en-US;q=0.9,en;q=0.8",
    "Host": "httpbin.org",
    "Upgrade-Insecure-Requests": "1",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36",
    "X-Amzn-Trace-Id": "Root=1-6065424b-3407a802406e5335774863e1"
  },
  "origin": "8.38.147.195",
  "url": "http://httpbin.org/get"
}
*/
/*
func (r *HttpResponseX) getRawHttp() string {

	var str = r.Proto + " " + r.Status + CRLF

	for name, values := range r.Header {
		count := len(values)
		for i, value := range values {
			str += name + ": " + value
			if i+1 != count {
				str += "; "
			}
		}
		str += CRLF
	}

	// bodyBytes, err := ioutil.ReadAll(r.Body)

	// if err == nil {
	// 	str += "Content-Length: " + strconv.Itoa(len(bodyBytes)) + CRLF + CRLF
	// 	r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
	// 	str += string(bodyBytes)
	// } else {
	str += CRLF
	// }

	return str
}
*/

package service

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/graphql-go/graphql"
	hel "github.com/hamza02x/go-helper"
	"gitlab.com/hamza02x/bilai-proxy/resolvers"
	"gitlab.com/hamza02x/bilai-proxy/types"
)

var (
	// fields       graphql.Fields
	// rootQuery    graphql.ObjectConfig
	//schemaConfig graphql.SchemaConfig
	schema graphql.Schema
)

func init() {

	fields := graphql.Fields{
		"requests": &graphql.Field{
			Type: graphql.NewList(types.RequestXType),
			Args: graphql.FieldConfigArgument{
				"page": &graphql.ArgumentConfig{Type: graphql.Int, DefaultValue: 1},
			},
			Resolve: resolvers.ResolverRequestXS,
		},
	}

	rootQuery := graphql.ObjectConfig{Name: "RootQuery", Fields: fields}
	schemaConfig := graphql.SchemaConfig{Query: graphql.NewObject(rootQuery)}
	schema, err = graphql.NewSchema(schemaConfig)

	if err != nil {
		fmt.Println("Error in graphsql schema", err)
		os.Exit(1)
	}
}

type GQuery struct {
	Query string `json:"query"`
}

func DoGraphQlQuery(c *gin.Context) {

	queryData, err := c.GetRawData()

	if err != nil {
		fmt.Println("Err", err)
		c.String(http.StatusInternalServerError, "Error parsing your data")
		return
	}

	var query GQuery
	if err := json.Unmarshal(queryData, &query); err != nil {
		fmt.Println("Err", err)
		c.String(http.StatusInternalServerError, "Error parsing your data")
		return
	}

	hel.Pl(query)

	params := graphql.Params{Schema: schema, RequestString: query.Query}

	r := graphql.Do(params)

	if len(r.Errors) > 0 {
		fmt.Printf("failed to execute graphql operation, errors: %#v\n", r.Errors)
	}

	rJSON, _ := json.Marshal(r)

	c.Data(200, "application/json", rJSON)
}

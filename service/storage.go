package service

import (
	"crypto/tls"
	"errors"
	"time"

	"github.com/dgraph-io/ristretto"
)

var (
	cache *ristretto.Cache
	err   error
)

type BialiCertStorage struct{}

func init() {
	cache, err = ristretto.NewCache(&ristretto.Config{
		NumCounters: 200,               // number of keys to track frequency of (10M).
		MaxCost:     1024 * 1024 * 100, // maximum cost of cache (100MB).
		BufferItems: 64,                // number of keys per Get buffer.
	})
	if err != nil {
		panic(err)
	}
}

func (tcs *BialiCertStorage) Fetch(hostname string, gen func() (*tls.Certificate, error)) (*tls.Certificate, error) {
	var cert *tls.Certificate
	var err error
	var ok bool

	value, ok := cache.Get(hostname)

	if ok {
		cert, ok = value.(*tls.Certificate)

		if !ok {
			cache.Del(hostname)
			err = errors.New("Error retrieveing from cache")
		}

	} else {

		cert, err = gen()

		if err != nil {
			return nil, err
		}

		cache.Set(hostname, cert, 1)
		time.Sleep(50 * time.Millisecond)
	}

	return cert, err
}

package formatter

import (
	"io"

	"github.com/alecthomas/chroma"
	"github.com/alecthomas/chroma/formatters/html"
	"github.com/alecthomas/chroma/lexers"
	"github.com/alecthomas/chroma/styles"
)

var (
	lexerHTTP     chroma.Lexer
	styleHTTP     *chroma.Style
	formatterHTTP *html.Formatter
)

func init() {
	formatterHTTP = html.New(html.Standalone(true))
	styleHTTP = styles.Register(chroma.MustNewStyle("monokai_custom", chroma.StyleEntries{
		chroma.Text:                "#f8f8f2",
		chroma.Error:               "#960050 bg:#1e0010",
		chroma.Comment:             "#75715e",
		chroma.Keyword:             "#66d9ef",
		chroma.KeywordNamespace:    "#f92672",
		chroma.Operator:            "#f92672",
		chroma.Punctuation:         "#f8f8f2",
		chroma.Name:                "#f8f8f2",
		chroma.NameAttribute:       "#a6e22e",
		chroma.NameClass:           "#a6e22e",
		chroma.NameConstant:        "#66d9ef",
		chroma.NameDecorator:       "#a6e22e",
		chroma.NameException:       "#a6e22e",
		chroma.NameFunction:        "#a6e22e",
		chroma.NameOther:           "#a6e22e",
		chroma.NameTag:             "#f92672",
		chroma.LiteralNumber:       "#ae81ff",
		chroma.Literal:             "#ae81ff",
		chroma.LiteralDate:         "#e6db74",
		chroma.LiteralString:       "#e6db74",
		chroma.LiteralStringEscape: "#ae81ff",
		chroma.GenericDeleted:      "#f92672",
		chroma.GenericEmph:         "italic",
		chroma.GenericInserted:     "#a6e22e",
		chroma.GenericStrong:       "bold",
		chroma.GenericSubheading:   "#75715e",
		chroma.Background:          "bg:#18222D",
	}))
	lexerHTTP = lexers.Get("http")

}

func FormatHTTP(w io.Writer, source string) error {

	if lexerHTTP == nil {
		lexerHTTP = lexers.Analyse(source)
	}

	if lexerHTTP == nil {
		lexerHTTP = lexers.Fallback
	}

	lexerHTTP = chroma.Coalesce(lexerHTTP)

	it, err := lexerHTTP.Tokenise(nil, source)

	if err != nil {
		return err
	}

	return formatterHTTP.Format(w, styleHTTP, it)
}

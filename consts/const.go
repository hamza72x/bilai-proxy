package consts

const (
	FILE_INTERCEPT = "intercept.txt"
	PROJECT_TEMP   = "temp"
	MimeJSON       = "JSON"
	MimeHTML       = "HTML"
	MimeJS         = "JavaScript"
	MimeCSS        = "CSS"
)

var (
	CRLF = []byte("\r\n")
	// DIR_TEMP              = "bilai-suite"
	DEFAULT_HOST_EXCLUDES = []string{
		"firefox-settings-attachments.cdn.mozilla.net",
		"firefox.settings.services.mozilla.com",
		"incoming.telemetry.mozilla.org",
		"fonts.googleapis.com",
		"fonts.gstatic.com",
		"pbs.twimg.com",
		"static.xx.fbcdn.net",
	}

	MAP_MIME_TYPE = map[string]string{
		"audio/aac":                           "AUDIO",
		"application/x-abiword":               "BINARY",
		"application/x-freearc":               "BINARY",
		"video/x-msvideo":                     "VIDEO",
		"application/vnd.amazon.ebook":        "BINARY",
		"application/octet-stream":            "BINARY",
		"image/bmp":                           "IMAGE",
		"application/x-bzip":                  "BINARY",
		"application/x-bzip2":                 "BINARY",
		"application/x-csh":                   "C-Shell Script",
		"text/css":                            MimeCSS,
		"text/csv":                            "CSV",
		"application/msword":                  "Microsoft Word",
		"application/vnd.ms-fontobject":       "FONT",
		"application/epub+zip":                "BINARY",
		"application/gzip":                    "BINARY",
		"image/gif":                           "IMAGE",
		"text/html":                           MimeHTML,
		"image/vnd.microsoft.icon":            "IMAGE",
		"image/x-icon":                        "IMAGE",
		"text/calendar":                       "iCalendar",
		"application/java-archive":            "BINARY",
		"image/jpeg":                          "IMAGE",
		"text/javascript":                     MimeJS,
		"application/javascript":              MimeJS,
		"application/json":                    MimeJSON,
		"application/ld+json":                 "JSON-LD",
		"audio/midi":                          "AUDIO",
		"audio/x-midi":                        "MIDI",
		"audio/mpeg":                          "AUDIO",
		"application/x-cdf":                   "AUDIO",
		"video/mp4":                           "VIDEO",
		"video/mpeg":                          "VIDEO",
		"audio/ogg":                           "AUDIO",
		"video/ogg":                           "VIDEO",
		"application/ogg":                     "AUDIO",
		"audio/opus":                          "AUDIO",
		"font/otf":                            "FONT",
		"image/png":                           "IMAGE",
		"application/pdf":                     "PDF",
		"application/x-httpd-php":             "Hypertext Preprocessor",
		"application/vnd.ms-powerpoint":       "Microsoft PowerPoint",
		"application/vnd.rar":                 "BINARY",
		"application/rtf":                     "RTF",
		"application/x-sh":                    "Bourne shell script",
		"image/svg+xml":                       "IMAGE",
		"application/x-tar":                   "BINARY",
		"image/tiff":                          "IMAGE",
		"video/mp2t":                          "VIDEO",
		"font/ttf":                            "FONT",
		"text/plain":                          "TEXT",
		"application/vnd.visio":               "Microsoft Visio",
		"audio/wav":                           "AUDIO",
		"audio/webm":                          "AUDIO",
		"video/webm":                          "VIDEO",
		"image/webp":                          "IMAGE",
		"font/woff":                           "FONT",
		"application/font-woff":               "FONT",
		"font/woff2":                          "FONT",
		"application/xhtml+xml":               "XHTML",
		"application/rss+xml":                 "XML",
		"application/vnd.ms-excel":            "Microsoft Excel",
		"application/xml":                     "XML",
		"text/xml":                            "XML",
		"application/zip":                     "BINARY",
		"video/3gpp":                          "VIDEO",
		"audio/3gpp":                          "AUDIO",
		"video/3gpp2":                         "VIDEO",
		"audio/3gpp2":                         "AUDIO",
		"application/x-7z-compressed":         "BINARY",
		"application/x-shockwave-flash":       "SWF",
		"application/vnd.apple.installer+xml": "Apple Installer Package",
		"application/vnd.mozilla.xul+xml":     "XUL",
		"application/vnd.oasis.opendocument.text":                                   "OpenDocument text document",
		"application/vnd.openxmlformats-officedocument.wordprocessingml.document":   "OpenXML",
		"application/vnd.oasis.opendocument.presentation":                           "OpenDocument presentation document",
		"application/vnd.oasis.opendocument.spreadsheet":                            "OpenDocument spreadsheet document",
		"application/vnd.openxmlformats-officedocument.presentationml.presentation": "Microsoft PowerPoint (OpenXML)",
		"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":         "Microsoft Excel (OpenXML)",
	}
)

func init() {
	// DIR_TEMP = strings.TrimSuffix(os.TempDir(), "/") + "/bilai-suite"
	// methods.RemoveAllContents(DIR_TEMP)
	// hel.DirCreateIfNotExists(DIR_TEMP)
}

#!/bin/bash
set -ex
# generate CA's  key
openssl genrsa -aes256 -passout pass:1 -out bilaiproxy.key.pem 4096
openssl rsa -passin pass:1 -in bilaiproxy.key.pem -out bilaiproxy.key.pem.tmp
mv bilaiproxy.key.pem.tmp bilaiproxy.key.pem

openssl req -config openssl.cnf -key bilaiproxy.key.pem -new -x509 -days 7300 -sha256 -extensions v3_ca -out bilaiproxy.pem

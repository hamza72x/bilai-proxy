package main

import (
	"context"
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/hamza02x/bilai-proxy/router"
	"gitlab.com/hamza02x/bilai-proxy/service"
)

var (
	proxyServer         *http.Server
	communicationServer *http.Server
)

func main() {

	flags()

	dbMigrate()

	test()

	go startProxyServer()

	startCommunicationServer()

}

// communication server

func startCommunicationServer() {

	if communicationServer != nil {
		communicationServer.Shutdown(context.Background())
	}

	communicationServer = &http.Server{
		Addr:    ":" + strconv.Itoa(communicationPort),
		Handler: router.GetWebRouter(),
	}

	communicationServer.ListenAndServe()
}

// proxy server

func startProxyServer() {

	if proxyServer != nil {
		proxyServer.Shutdown(context.Background())
	}

	proxy := NewProxyHttpServer()

	proxy.CertStore = &service.BialiCertStorage{}

	proxy.NonproxyHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, fmt.Sprintf("http://127.0.0.1:%d", communicationPort), http.StatusPermanentRedirect)
	})

	proxy.OnRequest().HandleConnect(AlwaysMitm)

	proxyServer = &http.Server{
		Addr:    ":" + strconv.Itoa(proxyPort),
		Handler: proxy,
	}

	proxyServer.ListenAndServe()
}

func orPanic(err error) {
	if err != nil {
		panic(err)
	}
}

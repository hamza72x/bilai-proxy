package main

import (
	"time"

	hel "github.com/hamza02x/go-helper"
	"gitlab.com/hamza02x/bilai-proxy/consts"
)

var (
	interceptCheck = make(chan int)
	MS_50          = 50 * time.Millisecond
)

func checkIntercepting() {
	go func() {
		if hel.FileExists(consts.FILE_INTERCEPT) {
			hel.Pl("Still intercepting")
			time.Sleep(300 * time.Millisecond)
			checkIntercepting()
		} else {
			interceptCheck <- 1
		}
	}()
}

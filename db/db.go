package db

import (
	"fmt"
	"os"
	"path/filepath"

	hel "github.com/hamza02x/go-helper"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var (
	conn *gorm.DB
	err  error
)

func GetConn() *gorm.DB {
	return conn
}

func Boot(dbPath string) {
	dbPath, err = filepath.Abs(dbPath)
	hel.Pl("boot", dbPath)

	if err != nil {
		fmt.Println("Error getting absolute path", err)
		os.Exit(1)
	}

	conn, err = gorm.Open(sqlite.Open(dbPath), &gorm.Config{})

	if err != nil {
		fmt.Println("Error setting database connection: " + err.Error())
		os.Exit(1)
	}

}

package model

type ResponseX struct {
	RequestXID uint   `gorm:"primaryKey;column:request_id" json:"request_id"`
	Header     string `json:"header"` // this will NOT have ending \r\n\r\n
	Body       string `json:"body"`
}

func (ResponseX) TableName() string {
	return TABLE_RESPONSES
}

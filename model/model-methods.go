package model

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"

	col "github.com/hamza02x/go-color"
	hel "github.com/hamza02x/go-helper"
	"github.com/yosssi/gohtml"
	"gitlab.com/hamza02x/bilai-proxy/consts"
	"gitlab.com/hamza02x/bilai-proxy/db"
	"gitlab.com/hamza02x/bilai-proxy/methods"
)

/***************************************************
					RequestX
***************************************************/

func (RequestX) GetMaxInsertedID() uint {
	var rx RequestX
	db.GetConn().Order("id desc").First(&rx)
	return rx.ID
}

// returns whether isn't insertable
// so that we dont' need to run consts.DEFAULT_HOST_EXCLUDES again for saving response
func (RequestX) Insert(httpReq *http.Request, isSSL bool) (bool, *RequestX) {

	if httpReq == nil {
		return false, nil
	}

	if hel.ArrStrContains(consts.DEFAULT_HOST_EXCLUDES, httpReq.Host) {
		return false, nil
	}

	var rx = &RequestX{
		Host:   httpReq.Host,
		Port:   httpReq.URL.Port(),
		Proto:  httpReq.Proto,
		IsSSL:  isSSL,
		Path:   methods.GetPathV1OfUrl(httpReq.URL),
		Method: httpReq.Method,
	}

	var bufHeader bytes.Buffer
	setHeader(&bufHeader, httpReq.Header)
	rx.Header = bufHeader.String()

	bodyBytes, err := ioutil.ReadAll(httpReq.Body)

	if err == nil {
		httpReq.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
		rx.Body = string(bodyBytes)
	}

	db.GetConn().Create(rx)

	fmt.Println(col.Green(rx.ID), rx.Host+rx.Path+"\n")

	return true, rx
}

func (rx *RequestX) UpdateResponse(resp *http.Response) {

	if resp == nil {
		return
	}

	rx.Length = int(resp.ContentLength)
	rx.Status = strconv.Itoa(resp.StatusCode)

	contentTypes := strings.Split(resp.Header.Get("Content-Type"), ";")

	if len(contentTypes) > 0 {
		if val, ok := consts.MAP_MIME_TYPE[contentTypes[0]]; ok {
			rx.Type = val
		} else if len(contentTypes[0]) > 1 {
			rx.Type = contentTypes[0]
		}
	}

	// for rx.Response
	var bufHeader bytes.Buffer

	fmt.Fprintf(&bufHeader, "%s %s%s", resp.Proto, resp.Status, consts.CRLF)
	setHeader(&bufHeader, resp.Header)
	rx.ResponseX.Header = bufHeader.String()

	bodyBytes, err := ioutil.ReadAll(resp.Body)

	if err == nil {
		resp.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
		rx.ResponseX.Body = string(bodyBytes)
		if rx.Length <= 0 {
			rx.Length = len(bodyBytes)
		}
	}

	db.GetConn().Save(rx)
}

func (rx *RequestX) GetRequestString() string {

	var buf strings.Builder

	fmt.Fprintf(&buf, "%s %s %s", rx.Method, rx.Path, rx.Proto)
	buf.Write(consts.CRLF)
	fmt.Fprintf(&buf, "Host: %s", rx.Host)
	buf.Write(consts.CRLF)
	buf.WriteString(rx.Header)
	buf.Write(consts.CRLF)
	buf.Write(consts.CRLF)
	buf.WriteString(rx.Body)

	return buf.String()
}

/***************************************************
					ResponseX
***************************************************/

func (respx *ResponseX) Beautify(dataType string) {

	fmt.Println("dataType = ", dataType)

	if dataType == consts.MimeJSON {
		var buf bytes.Buffer
		err := json.Indent(&buf, []byte(respx.Body), "", "    ")
		if err != nil {
			fmt.Println("Error making json format", err)
		}
		respx.Body = buf.String()

	} else if dataType == consts.MimeJS {

		tempJSFile, err := ioutil.TempFile("", "js.*.js")

		if err != nil {
			fmt.Println("Error getting temporary file", err)
			return
		}

		defer os.Remove(tempJSFile.Name())

		if _, err = tempJSFile.Write([]byte(respx.Body)); err != nil {
			fmt.Println(err.Error())
			return
		}

		fmt.Println("temp js file", tempJSFile.Name())

		cmd := exec.Command("js-beautify", tempJSFile.Name())

		stdout, err := cmd.Output()

		if err != nil {
			fmt.Println(err.Error())
			return
		}

		respx.Body = string(stdout)

	} else if dataType == consts.MimeHTML {

		respx.Body = gohtml.Format(respx.Body)

	}

}

// ************************** PRIVATE **************************** //

func setHeader(buf io.Writer, header http.Header) {

	var iHeader = 1
	var lenHeader = len(header)

	for k, values := range header {
		fmt.Fprintf(buf, "%s: %s", k, strings.Join(values, "; "))
		if iHeader != lenHeader {
			buf.Write(consts.CRLF)
		}
		iHeader++
	}

}

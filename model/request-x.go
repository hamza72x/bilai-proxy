package model

import (
	"time"
)

type RequestX struct {
	ID     uint   `gorm:"primaryKey" json:"id"`
	Host   string `json:"host"`
	Method string `json:"method"`
	Path   string `json:"path"`
	Status string `gorm:"default:--" json:"status"` // of response (200, 404)
	Length int    `gorm:"default:0" json:"length"`  // of response (content-length)
	Type   string `gorm:"default:--" json:"type"`   // of response (json, js, png|jpg, html, css, binary, --)

	Proto     string    `json:"proto"`
	Port      string    `json:"port"`
	IsSSL     bool      `json:"is_ssl"`
	Header    string    `json:"header"` // this will NOT have ending \r\n\r\n
	Body      string    `json:"body"`
	ResponseX ResponseX `gorm:"foreignKey:RequestXID"`

	// in case of modified request
	ParentRequestID       uint `json:"parent_request_id"`
	IsThisModifiedRequest bool `json:"is_this_modified_request"`

	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func (RequestX) TableName() string {
	return TABLE_REQUESTS
}

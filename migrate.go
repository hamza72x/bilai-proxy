package main

import (
	"gitlab.com/hamza02x/bilai-proxy/db"
	"gitlab.com/hamza02x/bilai-proxy/model"
)

func dbMigrate() {
	db.Boot(dbPath)
	db.GetConn().AutoMigrate(&model.RequestX{}, &model.ResponseX{})
}

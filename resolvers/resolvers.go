package resolvers

import (
	"errors"

	"github.com/graphql-go/graphql"
	"gitlab.com/hamza02x/bilai-proxy/db"
	"gitlab.com/hamza02x/bilai-proxy/model"
)

func ResolverRequestXS(p graphql.ResolveParams) (interface{}, error) {

	var requests []model.RequestX

	var page = 1
	var pageStr = p.Args["page"]

	if pageStr.(int) > 0 {
		page = pageStr.(int)
	}

	db.GetConn().Limit(10).Offset((page - 1) * 10).Find(&requests)

	return requests, nil
}

func ResolverResponseXFromRequestX(p graphql.ResolveParams) (interface{}, error) {
	rx, ok := p.Source.(model.RequestX)
	if ok {
		var resp model.ResponseX
		db.GetConn().Where("request_id = ?", rx.ID).First(&resp)
		return resp, nil
	}
	return nil, errors.New("Error parsing the source")
}
